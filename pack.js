

const shell = require('shelljs');
const fs = require('fs');
const path = require('path');
const gulp = require("gulp");

function log() {
  console.error(...arguments)
}

function start() {
  let rootPath = path.resolve("");
  let template = path.resolve("nwjs44_6");
  shell.exec(`cp -R ${template}/* ${rootPath}/dist/`);

}
function two() {
  let rootPath = path.resolve("");
  
  shell.exec(`cp -R ${rootPath}/assets ${rootPath}/dist/`);
  shell.exec(`cp -R ${rootPath}/csv ${rootPath}/dist/`);
  shell.exec(`cp -R ${rootPath}/csv_system ${rootPath}/dist/`);
  shell.exec(`cp -R ${rootPath}/index.html ${rootPath}/dist/`);
  shell.exec(`cp -R ${rootPath}/node_modules ${rootPath}/dist/`);
  shell.exec(`cp -R ${rootPath}/package.json ${rootPath}/dist/`);
  shell.exec(`cp -R ${rootPath}/src ${rootPath}/dist/`);
  shell.exec(`cp  ${rootPath}/README.pdf ${rootPath}/dist/`);
  
}
function three (){
  let rootPath = path.resolve("");
  shell.exec(`mv ${rootPath}/dist/nw.exe ${rootPath}/dist/run.exe`);
}
start();
two();
three()
// exports.default = gulp.series(start,  two()) ;
