const fs = require('fs');




function log( ) {
    console.log(...arguments);
}


function readFile (p) {
    return fs.readFileSync(p, { encoding : 'utf-8'});
}
function stringJson (json) {
    return JSON.stringify(json, null, 4);
}

module.exports = {
    log, 
    readFile,
    stringJson

}