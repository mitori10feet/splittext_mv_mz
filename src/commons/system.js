const fs = require('fs');
const path = require('path');
const { log, stringJson, readFile } = require('./utils.js');
const d3 = require('d3');




let systemPath = path.resolve('assets/System/origin');
let systemOut = path.resolve('assets/System/apply');
let systemCsvPath = path.resolve('csv_system');
/** 導出System */
function startOutSystem () {
    try {
        let result = fs.readdirSync(systemPath);
        
        let csvArray = ['type,id,name,nickname,profile,description'];
        
        let systemArray = ['文本說明,文本']
        let fianlData = "文本說明,文本";
        result.forEach( file => {
            if (file.match(/\.json$/)) {
                
                let originFullPath = `${systemPath}/${file}`;
                let fileJsonData = readFile(originFullPath);
                let fileObj = JSON.parse(fileJsonData);
                
                let allOutObj = {}
                
                for (let i = 0; i < fileObj.length; i ++) {
                    let outFileObj = null;
                    let data = fileObj[i];
                    if (!data) continue;
                    if (data.name) {
                        outFileObj = {
                            id : data.id,    
                            name : data.name || "",
                        }                                            
                        allOutObj[data.id] =  outFileObj;
                    }
                    if (data.profile) outFileObj[`profile`] = data.profile;
                    if (data.nickname) outFileObj[`nickname`] = data.nickname;
                    if (data.description) outFileObj[`description`] = data.description;
                    // let csvArray = ['jsonName', 'id', 'name', 'nickname','profile', 'description'];
                    let desc = data && data.description ? data.description.replace("\n",'\\n') : '';
                    let str = `${file.replace(".json","")},${data.id},${data.name||''},${data.nickname||''},${data.profile||''},${desc}`
                    csvArray.push(str);
                        
                    
                }
                if (file.match(/^System/)) {
                    let outFileObj = null;
                    let data = fileObj;
                    outFileObj = {
                        "盔甲類型" : data.armorTypes,
                        "金錢單位" : data.currencyUnit,
                        "屬性" : data.elements,
                        "裝備類型" : data.equipTypes,
                        "遊戲名稱" : data.gameTitle,
                        "技能類型" : data.skillTypes,
                        "系統" : data.terms,
                        "武器類型": data.weaponTypes,
                    }
                    allOutObj = outFileObj;

                    let fakeObj = Object.assign({},outFileObj);
                    // log(fakeObj);
                    fianlData +=  objToString('', fakeObj);
                    systemArray.push('');
                    // log(`STRING`, fianlData);
            
                }

                let allOutJson = stringJson(allOutObj);
                
                
                if (!fs.existsSync(systemOut)) {
                    fs.mkdirSync(systemOut);
                }            
                fs.writeFileSync(`${systemOut}/${file}`, allOutJson, { encoding : 'utf-8'});
            }
        })
        
        if (csvArray.length >= 2) {
            let stringOther = csvArray.join("\n");
            fs.writeFileSync(`${systemCsvPath}/Other.csv`, stringOther, { encoding: 'utf-8'});
        }

        
        if (systemArray.length >= 2) {
            log(fianlData)
            // let systemString = systemArray.join("\n")
            fs.writeFileSync(`${systemCsvPath}/System.csv`, fianlData, { encoding: 'utf-8'});
        }
        
        alert('輸出系統完畢')
        }   
    catch (e) {
        
        alert('沒有做防呆，請按照說明書的指示做')
        
    }
    
}

function oneJoinType (key) {
    return ['盔甲類型','屬性','技能類型','武器類型', 'basic', 'commands','params'].some( s => {
        return s == key;
    });
}
function justString ( key ) {
    return ['遊戲名稱','金錢單位'].some( s => {
        return key == s;
    })
}

function objToString (newStr, obj){

    for (let k in obj) {
        if (oneJoinType(k)) {            
            let arr = obj[k].join("';");
            log(`武器`,arr);
            newStr += `\n${k},${arr}`;
        }
        else if (justString(k)){
            newStr += `\n${k},${obj[k]}`;
        }
        else if (k == '系統'){
            for (let j in obj['系統']) {
                if (oneJoinType(j)) {
                    let arr = obj[k][j].join("';");
                    newStr += `\n${k}-${j},${arr}`;
                }
                else {
                    Object.keys( obj['系統']['messages']).forEach( k2 => {
                        newStr += `\n系統-messages-${k2},${obj['系統']['messages'][k2]}`;
                    })
                }
            }
        }


    }
    return newStr;
}


const MAPP_SYSTEM = {
    "盔甲類型" : 'armorTypes',
    "金錢單位" : 'currencyUnit',
    "屬性" : 'elements',
    "裝備類型" : 'equipTypes',
    "遊戲名稱" : 'gameTitle',
    "技能類型" : 'skillTypes',
    "系統" : 'terms',
    "武器類型": 'weaponTypes',    
}
let originSytemPath = path.resolve('assets/System/origin');
let applySystemPath = path.resolve('assets/System/apply');
function applySystem () {
    let result = fs.readdirSync(applySystemPath);
    result.forEach( file => {
        if (file.match(/\.json$/)) {
            /** 讀取現有檔案 */
            let originFileJson = readFile(`${originSytemPath}/${file}`);
            let originObj = JSON.parse(originFileJson);
            let applyFileJson = readFile(`${applySystemPath}/${file}`);
            let applyFileObj = JSON.parse(applyFileJson);
            
            
            if (file.match(/^System/)) {

                for (let k in applyFileObj) {
                    let originKey = MAPP_SYSTEM[ k ];
                    originObj[ originKey ] = applyFileObj[k];
                }    
            }
            else {
                for (let k in applyFileObj) {
                    let applyId = Number(k);
                    originObj.forEach( (data, index) => {
                        if (data && data.id == applyId) {
                            let apply = applyFileObj[k];
                            
                            if (data.name) data.name = apply.name;
                            if (data.profile) data.profile = apply.profile;
                            if (data.nickname) data.nickname = apply.nickname;
                            if (data.description) data.description = apply.description;
                            log(`apply.name , ${apply.name}`)

                        }
                    })

                }
            }


            let finalJson = stringJson(originObj);
            fs.writeFileSync(`${originSytemPath}/${file}`, finalJson, { encoding: 'utf-8'} );                
        }
    })
    log(result)

}

let csvSystemPath = path.resolve('csv_system');


function applySystemCsvToJson () {
    try {
        let result = fs.readdirSync(csvSystemPath);
        let applySystemCsvToJsonPath = path.resolve('assets/System/apply');

        log(result);
        let systemHash = {};
        let otherHash = {};
        result.forEach( csvFile => {
            if (csvFile.match(/\.csv/)) {

                let csvPath =  path.resolve(`${csvSystemPath}/${csvFile}`);
                let csvText = fs.readFileSync(csvPath, { encoding: 'utf-8'});
                // /** CSV 處理 */
                if (csvFile.match(/System.csv/)) {
                    
                    // log(csvText);
                    let csvArray = d3.csvParse(csvText) 
                    // log(csvArray);

                    csvArray.forEach( csvObj => {
                        let bigKey = csvObj['文本說明'];
                        
                        if (oneJoinType(bigKey)) {
                            systemHash[bigKey] = csvObj['文本'].split("';");
                        }
                        else if (justString(bigKey)) {
                            systemHash[bigKey] = csvObj['文本'];
                        }
                        else if (bigKey.match(/^系統/)) {
                            let arr = bigKey.split("-");
                            systemHash['系統'] = systemHash['系統'] || {};

                            if (oneJoinType(arr[1])) {
                                systemHash['系統'][ arr[1] ] = csvObj['文本'].split("';");                            
                            }
                            else if (arr[1] == 'messages'){
                                systemHash['系統'][ arr[1] ] = systemHash['系統'][ arr[1] ] || {}
                                systemHash['系統'][ arr[1] ][arr[2]] = csvObj['文本'];

                            }    
                        }
                    })
                }
                /** 其他術語 */
                if (csvFile.match(/Other.csv/)) {
                    let csvArray = d3.csvParse(csvText) 
                    
                    csvArray.forEach( dataHash => {
                        log(`dataHash['type'] ${dataHash['type']}`);
                        otherHash[ dataHash['type'] ] = otherHash[ dataHash['type'] ] || {};
                        otherHash[ dataHash['type'] ][ dataHash['id'] ] =  otherHash[ dataHash['type'] ][ dataHash['id'] ] || {}; 
                        /** 開始匯入 */
                        // otherHash[ dataHash['type'] ][ dataHash['id'] ] = dataHash['id'];
                        if (dataHash['name']) {
                            otherHash[ dataHash['type'] ][ dataHash['id'] ]['name'] = dataHash['name'];
                        }
                        if (dataHash['nickname']) {
                            otherHash[ dataHash['type'] ][ dataHash['id'] ]['nickname'] = dataHash['nickname'];
                        }
                        if (dataHash['profile']) {
                            otherHash[ dataHash['type'] ][ dataHash['id'] ]['profile'] = dataHash['profile'];
                        }
                        if (dataHash['description']) {
                            otherHash[ dataHash['type'] ][ dataHash['id'] ]['description'] = dataHash['description'];
                        }                                                            
                    })


                }


            }
        })
        if (Object.keys(systemHash).length > 0) {
            let systemJson = stringJson(systemHash);
            fs.writeFileSync(`${applySystemCsvToJsonPath}/System.json`, systemJson, {encoding : 'utf-8'});
        }
        Object.keys(otherHash).forEach( key => {
            let data = stringJson(otherHash[key]);
            fs.writeFileSync(`${applySystemCsvToJsonPath}/${key}.json`, data, {encoding : 'utf-8'});
        })
        applySystem();
        alert('導入系統完畢')
    }
    catch(e) {
        alert(`沒做防呆，請按照說明書操作`)
    }

}


module.exports = {
    startOutSystem,
    applySystem,
    applySystemCsvToJson,
    
}