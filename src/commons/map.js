const fs = require('fs');
const path = require('path');
const { log, stringJson } = require('./utils.js');
const d3 = require('d3');
  



let filePath = path.resolve('assets/Map/origin');
let out = path.resolve('assets/Map/apply');
const CODE_TEXT_DATA = 401;
const CODE_TEXT_NAME = 101; // param[4];
const CODE_TEXT_CHOICES = 102; // parameters[0]
const CODE_TEXT_SCROLL = 405; // parameters[0]
const MATCH_CODE = [
    CODE_TEXT_DATA, CODE_TEXT_NAME, CODE_TEXT_CHOICES, CODE_TEXT_SCROLL
]


function startSeachOutput () {
    try {
        let result = fs.readdirSync(filePath);
        var all = []
        result.forEach( str => {
            log(str);
            if (str.match(/CommonEvents.json/)) {
                let fileName = `${filePath}/${str}`;
                let mapId = 'CommonEvents'
                let dirName = `${out}/${str.replace('.json','')}`;
                startCommand(fileName, dirName ,mapId)
            }
            else if (str.match(/^Map/) ) {
                log(str);
                let fileName = `${filePath}/${str}`;   
                let mapid = Number((str.replace(/^Map/, "")).replace(/\.json$/,""));
                
                start(fileName, `${out}/${str.replace('.json','')}` , mapid,all);
            }
        })
        alert(`輸出完畢`);
    }
    catch(e) {
        log(e)
        alert('沒有做防呆，請按照說明書的指示做')
    }
    let csvString = all.join("\n");
    if (!fs.existsSync('csv')) {
        fs.mkdirSync('./csv');
    }
    fs.writeFileSync(`./csv/all.csv`, csvString, { encoding: 'utf-8'}); 
    
    // log(result)
}
function startCommand(fp, newDirName, mapid) {
    let json = fs.readFileSync(fp, { encoding : 'utf-8'});
    let fileOriginObj = JSON.parse(json);
    let text_csv = [ 
        "地圖,事件id,list,類型,代碼,文本"
    ]    
    
    fileOriginObj.forEach( (data, index) => {
        
        if (data && data.list) {
            // log(data);
            let eventId = data.id;
            let tempArray = data.list;
            let obj = {};
            let has_text = false;
            tempArray.forEach( (d2, index2) => {
                let code = d2.code;
                
                let parameters = d2.parameters;
                
                if (MATCH_CODE.some( c => {return c == code})) {
                    obj[eventId] = obj[eventId] || {};

                    let tip = codeTip(code);                    
                    let text = paramText(code, parameters);

                    
                    
                    let push_text;
                    if (text && typeof text == 'object') {                        
                        push_text = text.join("';");
                    }
                    else {
                        push_text = text;
                    }

                    if (push_text) {
                        has_text = true;
                        obj[eventId][`list_${index2}`] = obj[eventId][`list_${index2}`] || {};
                        obj[eventId][`list_${index2}`]['code'] = code;
                        obj[eventId][`list_${index2}`]['tip'] = tip                        
                        obj[eventId][`list_${index2}`]['text'] = push_text;
                        text_csv.push(`${mapid},${eventId},${index2},${tip},${code},${push_text}`);
                    }



                }
            })
            if (!fs.existsSync(newDirName)) {
                log(`newDirName ${newDirName}`)
                fs.mkdirSync(newDirName);
            }        
            if (has_text) {
                let outJson = JSON.stringify(obj, null, 4);
                fs.writeFileSync(`${newDirName}/${eventId}.json`, outJson, { encoding : 'utf-8'});        
            }

        }        

    })
    /** 儲存地圖的csv */
    let csvString = text_csv.join("\n");
    if (!fs.existsSync('csv')) {
        fs.mkdirSync('./csv');
    }    
    fs.writeFileSync(`./csv/CommonEvents.csv`, csvString, { encoding: 'utf-8'});    


        
    // log(text_csv);
}

function start (fp, newDirName, mapid,all ) {    
    let json = fs.readFileSync(fp, { encoding : 'utf-8'});
    let fileOriginObj = JSON.parse(json);
    let allEvents = fileOriginObj['events'];
    

    let text_csv = [ 
        "地圖,事件id,頁數,list,類型,代碼,文本"
    ]    
    if (all.length == 0) {
        all.push("地圖,事件id,頁數,list,類型,代碼,文本");
    }


    for (let i = 1; i < allEvents.length; i ++ ) {
        let obj = null;
        let event = allEvents[i];
        if (!event) {
            continue;
        }
        let eventId = event.id;
        obj = {
            id : eventId,
        }
        let pages = event.pages;
        for (let j = 0; j < pages.length; j ++ ) {
            let page = pages[j];
            let list = page.list;

            for (let k = 0; k < list.length; k++) {
                let data = list[k];
                let code  = data.code ;
                let parameters = data.parameters;
                if (MATCH_CODE.some( c => {return c == code})) {                    
                    obj[`pages_index_${j}`] = obj[`pages_index_${j}`] || {}
                    obj[`pages_index_${j}`][`list_index_${k}`] = obj[`pages_index_${j}`][`list_index_${k}`] || {};

                    let key = codeTip(code);                    
                    let text = paramText(code, parameters);
                    if (text) obj[`pages_index_${j}`][`list_index_${k}`].text = text;
                    obj[`pages_index_${j}`][`list_index_${k}`].tip = key;
                    obj[`pages_index_${j}`][`list_index_${k}`].code =code;
                
                    // "地圖,事件id,頁數,list(你別管這是啥),類型,代碼,文本"
                    obj.hasText = true;
                    
                    let push_text;
                    if (text && typeof text == 'object') {                        
                        push_text = text.join("';");
                    }
                    else {
                        push_text = text;
                    }
                    if (push_text) {
                        let push_event = `${mapid},${eventId},${j},${k},${key},${code},${push_text}`;
                        text_csv.push(push_event);
                        all.push(push_event);
                    }
                    
                    


                }
                
            }

            if (obj.hasText) {
                if (!fs.existsSync(newDirName)) {
                    log(`newDirName ${newDirName}`)
                    fs.mkdirSync(newDirName);
                }
                let outJson = JSON.stringify(obj, null, 4);
                fs.writeFileSync(`${newDirName}/${eventId}.json`, outJson, { encoding : 'utf-8'});
            }
        }
        


    }
    /** 儲存地圖的csv */
    let csvString = text_csv.join("\n");
    if (!fs.existsSync('csv')) {
        fs.mkdirSync('./csv');
    }
    // fs.writeFileSync(`./csv/Map${mapid}.csv`, csvString, { encoding: 'utf-8'});    
    return all;

}

function paramText(code , parameters) {
    
    let text = '';
    switch (code) {
        case CODE_TEXT_NAME: 
            text = parameters[4];
            break;                   
        case CODE_TEXT_SCROLL: 
        case CODE_TEXT_DATA:         
            text = parameters[0];        
            break;
        case CODE_TEXT_CHOICES:          
            text = []
            parameters[0].forEach( s => {
                text.push(s);
            } )
            break;                        
    }
    return text;

}

function codeTip (code) {
    let tip = ''
    switch (code) {
        case CODE_TEXT_DATA: 
            tip = '文本'
            break;                   
        case CODE_TEXT_NAME: 
            tip = '人名'
            break;                   
        case CODE_TEXT_CHOICES: 
            tip = '選項'
            break;                   
        case CODE_TEXT_SCROLL: 
            tip = '滾動文字'
            break;                        
    }
    return tip
}




let applyFilePath = path.resolve('assets/Map/apply');
let applyOutPath = path.resolve('assets/Map/origin');
function startApplyFileDir () {
    let result = fs.readdirSync(applyFilePath);
    
    
    result.forEach( str => {

        
            /** 讀取看有多少資料要apply  */
        let dirPath = `${applyFilePath}/${str}`;
        
        // return;
        let stat = fs.lstatSync(dirPath);
        if (stat.isDirectory()) {
           

            let result2 = fs.readdirSync(`${applyFilePath}/${str}`);
            
            /** 要合併回去的檔案 */
            let applyFileName = `${applyOutPath}/${str}.json`;
            
            
            let jsonFile = fs.readFileSync(applyFileName, { encoding : 'utf-8'});            
            let jsonObj = JSON.parse(jsonFile);            
            

            if (str == 'CommonEvents') {
                result2.forEach( eventJson => {                
                    if (eventJson && eventJson.match(/json$/)) {
                        let fileFullName= `${applyFilePath}/${str}/${eventJson}`;                                    
                        let eJson = fs.readFileSync( fileFullName, { encoding: 'utf-8'});
                        
                        let eObj = JSON.parse(eJson);
                        Object.keys(eObj).forEach( applyId => {
                            let originObj = jsonObj.find( data => {
                                return data && data.id == applyId;
                            })
                            let newListObj = eObj[applyId];
                            Object.keys(newListObj).forEach( listKey => {
                                let arrKey = listKey.split("_");
                                    
                                let text = newListObj[listKey].text;
                                let code = newListObj[listKey].code;
                          

                                if (code == CODE_TEXT_NAME) {
                                    originObj.list[ arrKey[1] ].parameters[4] = text;
                                }
                                else if (code == CODE_TEXT_SCROLL || code == CODE_TEXT_DATA) {
                                    originObj.list[ arrKey[1] ].parameters[0] = text;
                                }
                                else if (code == CODE_TEXT_CHOICES) {
                                    originObj.list[ arrKey[1] ].parameters[0] = text;
                                }                                                                                              
                            })
                        })
                    }
                });
        
                let allNewJson = JSON.stringify(jsonObj);
                fs.writeFileSync(applyFileName  , allNewJson,{ encoding : 'utf-8'})                                      
            }
            else {
                result2.forEach( eventJson => {                
                    if (eventJson && eventJson.match(/json$/)) {
    
                        
                        let fileFullName= `${applyFilePath}/${str}/${eventJson}`;                                    
                        let eJson = fs.readFileSync( fileFullName, { encoding: 'utf-8'});
                        
                        let eObj = JSON.parse(eJson);
                        let id = eObj.id;
                        
                        let originObj = jsonObj.events.find( data => {
                            return data && data.id == id;
                        })
                        delete eObj.hasText;
                        delete eObj.id;
    
                        Object.keys(eObj).forEach(key => {                        
                                let arrPage = key.split("_");       
    
    
                                /** 需要被取代的obj  */                     
                                let needReplaceObjPage = originObj.pages[ arrPage[2] ];
    
                                let newListObj = eObj[key];
                                
                                
                                Object.keys(newListObj).forEach( listKey => {
                                    let arrKey = listKey.split("_");
                                    
                                    let text = newListObj[listKey].text;
                                    let code = newListObj[listKey].code;
                                    // log(newListObj)

                                    if (code == CODE_TEXT_NAME) {
                                        
                                        needReplaceObjPage.list[ arrKey[2] ].parameters[4] = text;
                                    }
                                    else if (code == CODE_TEXT_SCROLL || code == CODE_TEXT_DATA) {
                                        if (!needReplaceObjPage.list[ arrKey[2] ]) {
                                            console.error(`id ${id}`, fileFullName);
                                        }
                                        
                                        needReplaceObjPage.list[ arrKey[2] ].parameters[0] = text;
                                    }
                                    else if (code == CODE_TEXT_CHOICES) {
                                        needReplaceObjPage.list[ arrKey[2] ].parameters[0] = text;
                                    }                                                            
                                })      
                        })
                        
                        // log(eObj);
                    }
                })
                let allNewJson = JSON.stringify(jsonObj);
                fs.writeFileSync(applyFileName  , allNewJson,{ encoding : 'utf-8'})                      
            }


      
        }
    })
}




const csvDir = path.resolve('csv')
function applyMapCsv () {
    try {
        let result = fs.readdirSync(csvDir);
        let writePathBasic = path.resolve('assets/Map')
        
        result.forEach( csvFile => {
            
            if (csvFile.match(/\.csv/)) {

                
                
                let csvPath =  path.resolve(`csv/${csvFile}`);
                let csvText = fs.readFileSync(csvPath, { encoding: 'utf-8'});
                let csvArray = d3.csvParse(csvText)        
                
                

                if (csvFile.match(/CommonEvents/)) {
                    let allHash = {};
                    csvArray.forEach( csvObj => {
                        let eventId = csvObj[`事件id`];
                        // let mapId = csvObj[`地圖`];
                        // let pagesIndex = csvObj[`頁數`];
                        let listIndex = csvObj[`list`];
                        let tip = csvObj[`類型`];
                        let code = csvObj[`代碼`];
                        let text;
    
                        if (tip == '選項') {
                            text = csvObj[`文本`].split("';");;
                        }
                        else {
                            text = csvObj[`文本`];
                        }                        
                        allHash['id'] = allHash['id'] || {};
                        allHash['id'][eventId] = allHash['id'][eventId] || {};
                        allHash['id'][eventId][`list_${listIndex}`] = {                            
                            tip ,
                            code,
                            text,                            
                            
                        }

                        let dirName = `CommonEvents`;                
                        for (let k in allHash[`id`]) {
                            let fileName = `${k}.json`;
                            let fullPath =  `${writePathBasic}/apply/${dirName}/${fileName}`;
                            let writeData = { } ;
                            writeData[k] = allHash[`id`][k];
                           
                            let json = stringJson(writeData)
                            fs.writeFileSync(fullPath, json, { encoding : 'utf-8'});            
                        }                        
               
                        // log(allHash['id'][eventId][`list_${listIndex}`]);
                    })
                    

                }
                else {
                    let splitHash = {};
                    csvArray.forEach( csvObj => {
                        let mapId = csvObj[`地圖`];
                        splitHash[mapId] = splitHash[mapId] || [];
                        splitHash[mapId].push(csvObj);    
                    })

                    for (let key in splitHash) {
                        let allHash = {};
                        splitHash[key].forEach( csvObj => {
                            let eventId = csvObj[`事件id`];
                            let mapId = csvObj[`地圖`];
                            let pagesIndex = csvObj[`頁數`];
                            let listIndex = csvObj[`list`];
                            let tip = csvObj[`類型`];
                            let code = csvObj[`代碼`];
                            let text;
        
                            
        
                            if (tip == '選項') {
                                text = csvObj[`文本`].split("';");;
                            }
                            else {
                                text = csvObj[`文本`];
                            }
        
                            allHash['mapId'] = mapId
                            allHash['id'] = allHash['id'] || {} 
                            allHash['id'][eventId] = allHash['id'][eventId] || {};
                            allHash['id'][eventId][`pages_index_${pagesIndex}`] = allHash['id'][eventId][`pages_index_${pagesIndex}`] || {};
                            allHash['id'][eventId][`pages_index_${pagesIndex}`][`list_index_${listIndex}`] = allHash['id'][eventId][`pages_index_${pagesIndex}`][`list_index_${listIndex}`] || {};
        
        
                            allHash['id'][eventId][`pages_index_${pagesIndex}`][`list_index_${listIndex}`] = {
                                tip ,
                                code,
                                text,
                            }            


                        })
                        // log(allHash);
                        let mapId = allHash[`mapId`];
                        delete allHash[`mapId`];
                        let dirName = `Map` + `${mapId}`.padStart(3, "0");                
                        for (let k in allHash[`id`]) {
                            let fileName = `${k}.json`;
                            let fullPath =  `${writePathBasic}/apply/${dirName}/${fileName}`;
                            let writeData = allHash[`id`][k];
                            writeData.id = k;
                            let json = stringJson(writeData)
                            // alert(fullPath)
                            fs.writeFileSync(fullPath, json, { encoding : 'utf-8'});            
                        }                        
                        
                    }
        

                }

            
            }
        })
        startApplyFileDir();
        alert('導入地圖完畢')
    }
    catch(e) {
        alert(e)
        console.log(`${e}`)
    }

    


}








module.exports = {
    startSeachOutput,
    startApplyFileDir,
    applyMapCsv,
}

